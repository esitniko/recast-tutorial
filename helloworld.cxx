#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{
  std::cout << "Hello World. We will write a message to " << argv[1] << std::endl;
  std::ofstream myfile(argv[1]);
  myfile << "Writing this to a file. My message is " << argv[2] << std::endl;
  myfile.close();
  return 0;
}
